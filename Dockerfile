FROM node:16

# Create app directory
WORKDIR /app

# Install app dependencies
COPY package.json yarn.lock ./
RUN yarn install --production

# Bundle app source
COPY . .
EXPOSE 3000
CMD ["node", "src/index.js"]