const {add, subtract} = require('./index');

test('add 2 + 1 to equal 3', () => {
    expect(add(2, 1)).toBe(3);
});

test('subtract 2 - 1 to equal 1', () => {
    expect(subtract(2, 1)).toBe(1);
});