const express = require('express');
const app = express();

app.get('/', (req, res) => {
    const a = add(1,2);
    const b = subtract(2,1);
    res.send(`results: 1+2=${a} 2-1=${b}`);
});

function add(a, b) {
    return a+b;
}

function subtract(a,b) {
    return a-b;
}

module.exports = { add, subtract, app };